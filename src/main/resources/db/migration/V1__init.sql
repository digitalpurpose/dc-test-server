create table users (
    id                           bigserial not null primary key,
    email                        varchar(255) not null,
    password_enc                 varchar(255) not null,
    first_name                   varchar(255) not null,
    last_name                    varchar(255) not null,
    status                       varchar(255) not null,
    role                         varchar(255) not null,
    
    confirmation_token           varchar(255),
    reset_password_token         varchar(255),
    reset_password_token_expiry  timestamptz,
    
    last_login                   timestamptz,
    last_password_update         timestamptz,
    first_failed_login_at        timestamptz,
    account_locked_at            timestamptz,
    num_failed_logins            int default 0,
    
    created_by                   bigint,
    created_on                   timestamptz,
    updated_by                   bigint,
    updated_on                   timestamptz
);

create index idx_user_status on users(status);
create index idx_user_role on users(role);

insert into users (email, first_name, last_name, status, role, password_enc) values ('tech+dp@digitalpurpose.com.au', 'admin', 'admin', 'active', 'admin', '$2a$10$LxKSxD8HD3Dy1ZSEpo8rV.FFehgd.lILL002epXj41ITvg9askvv6');
