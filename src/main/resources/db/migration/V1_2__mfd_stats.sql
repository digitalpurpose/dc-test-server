create view mfd_week as 
select 
  user_device_id
, mfd_date
, d.created_date 
, case 
    when mfd_date >= d.created_date and mfd_date < d.created_date + interval '1 week' then 1
    when mfd_date >= d.created_date + interval '1 week' and mfd_date < d.created_date + interval '2 week' then 2
    when mfd_date >= d.created_date + interval '2 week' and mfd_date < d.created_date + interval '3 week' then 3
    when mfd_date >= d.created_date + interval '3 week' and mfd_date < d.created_date + interval '4 week' then 4
    when mfd_date >= d.created_date + interval '4 week' and mfd_date < d.created_date + interval '5 week' then 5
    else 9
  end as week
from mfd m 
join user_device d on m.user_device_id = d.id   
order by 2, 3;

create view mfd_stat as 
select 
   row_number() OVER () AS id
,  user_device_id
, count(distinct week) as num_weeks
, sum(total_week) as total
, round(sum(total_week) / count(distinct week), 2) as avg_week 
from (
	select user_device_id
	, week
	, count(1) as total_week
	from mfd_week 
	group by user_device_id, week
) as x
group by user_device_id;

