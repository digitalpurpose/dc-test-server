create table user_device (
    id                           bigserial not null primary key,
    identifier                   varchar(1000) not null,
    created_date                 timestamptz
);

create index idx_device_identifier on user_device(identifier);

create table mfd (
    id                           bigserial not null primary key,
    user_device_id               bigint not null references user_device,
    mfd_date                     timestamptz
);

create index idx_mfd_userid on mfd(user_device_id);
