package au.com.digitalpurpose.dctest.server.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import au.com.digitalpurpose.dctest.server.exception.NoSuchObjectException;
import au.com.digitalpurpose.dctest.server.model.MeatFreeDayStat;
import au.com.digitalpurpose.dctest.server.model.User;
import au.com.digitalpurpose.dctest.server.model.UserDevice;
import au.com.digitalpurpose.dctest.server.repository.MeatFreeDayStatRepository;
import au.com.digitalpurpose.dctest.server.repository.UserDeviceRepository;
import au.com.digitalpurpose.dctest.server.request.user.SearchUserDevicesRequest;

@Service
public class UserDeviceService {

    private final UserDeviceRepository userDeviceRepository;
    private final MeatFreeDayStatRepository meatFreeDayStatRepository;

    @Autowired
    public UserDeviceService(UserDeviceRepository userDeviceRepository,
                             MeatFreeDayStatRepository meatFreeDayStatRepository) {
        this.userDeviceRepository = userDeviceRepository;
        this.meatFreeDayStatRepository = meatFreeDayStatRepository;
    }

    public Page<UserDevice> filter(SearchUserDevicesRequest request) {
        Page<UserDevice> devices = userDeviceRepository.findAll(request.getPageRequest());
        
        // get stats for devices
        Set<Long> deviceIds = devices.stream().map(UserDevice::getId).collect(Collectors.toSet());
        List<MeatFreeDayStat> stats = meatFreeDayStatRepository.findForDeviceIds(deviceIds);
        Map<Long, MeatFreeDayStat> statsPerDevice = stats.stream().collect(Collectors.toMap(MeatFreeDayStat::getUserDeviceId, Function.identity()));
        devices.forEach(d -> {
            MeatFreeDayStat stat = statsPerDevice.get(d.getId());
            if (stat != null) {
                d.setAverageMfdPerWeek(stat.getAveragePerWeek());
                d.setTotalMfd(stat.getTotal());
            }
        });
        
        return devices;
    }

    public UserDevice get(String identifier) throws NoSuchObjectException {
        return userDeviceRepository.findByIdentifier(identifier).orElseThrow(() -> new NoSuchObjectException(UserDevice.class, identifier));
    }

}
