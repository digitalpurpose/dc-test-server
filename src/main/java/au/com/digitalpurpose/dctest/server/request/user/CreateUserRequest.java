package au.com.digitalpurpose.dctest.server.request.user;

public class CreateUserRequest extends AbstractUserRequest {

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
