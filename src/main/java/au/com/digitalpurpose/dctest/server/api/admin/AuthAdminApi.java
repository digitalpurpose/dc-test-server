package au.com.digitalpurpose.dctest.server.api.admin;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import au.com.digitalpurpose.dctest.server.exception.AuthenticationException;
import au.com.digitalpurpose.dctest.server.exception.InvalidResetPasswordTokenException;
import au.com.digitalpurpose.dctest.server.request.auth.LoginRequest;
import au.com.digitalpurpose.dctest.server.request.user.CheckResetForgotPasswordTokenRequest;
import au.com.digitalpurpose.dctest.server.request.user.ForgotPasswordRequest;
import au.com.digitalpurpose.dctest.server.request.user.ResetForgottenPasswordRequest;
import au.com.digitalpurpose.dctest.server.security.JwtTokenProvider;
import au.com.digitalpurpose.dctest.server.security.Token;
import au.com.digitalpurpose.dctest.server.service.AuthService;
import au.com.digitalpurpose.dctest.server.service.AuthService.UserAccountWithTokens;
import au.com.digitalpurpose.dctest.server.util.dto.DtoBuilder;

@RestController
@RequestMapping("/api/admin/auth")
public class AuthAdminApi {

    private final AuthService authService;
    private final JwtTokenProvider jwtTokenProvider;

    @Autowired
    public AuthAdminApi(AuthService authService,
                        JwtTokenProvider jwtTokenProvider) {
        this.authService = authService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping
    public Map<String, Object> login(@RequestBody LoginRequest request) throws AuthenticationException {
        UserAccountWithTokens auth = authService.loginUser(request);
        Map<String, Object> result = new HashMap<>();
        result.put("token", auth.getToken());
        result.put("refresh_token", auth.getRefreshToken());
        result.put("user", userDto.build(auth.getUser()));
        return result;
    }

    @PostMapping("keepalive")
    public Map<String, Object> keepAlive(@RequestBody LoginRequest request, HttpServletRequest servletRequest) throws AuthenticationException {
        UserAccountWithTokens auth = authService.loginUser(request);
        Map<String, Object> result = new HashMap<>();
        result.put("token", auth.getToken());
        result.put("refresh_token", auth.getRefreshToken());
        result.put("user", userDto.build(auth.getUser()));
        return result;
    }

    @PostMapping("/refresh")
    public Map<String, Object> refreshToken(@RequestBody String refreshToken) {
        Token token = jwtTokenProvider.refreshToken(refreshToken);
        Map<String, Object> result = new HashMap<>();
        result.put("token", token.getToken());
        result.put("refresh_token", token.getRefreshToken());
        return result;
    }

    @PostMapping("forgot-password")
    public ResponseEntity<Object> forgotPassword(@Valid @RequestBody ForgotPasswordRequest request) {
        authService.forgotPassword(request);
        return ResponseEntity.ok().build();
    }

    @PostMapping("reset-forgotten-password")
    public ResponseEntity<Object> resetForgottenPassword(@Valid @RequestBody ResetForgottenPasswordRequest request)
            throws InvalidResetPasswordTokenException {
        authService.resetForgotPassword(request);
        return ResponseEntity.ok().build();
    }

    @PostMapping("reset-forgotten-password/check")
    public ResponseEntity<Object> checkResetForgottenPasswordToken(@Valid @RequestBody CheckResetForgotPasswordTokenRequest request)
            throws InvalidResetPasswordTokenException {
        authService.checkResetForgotPasswordToken(request);
        return ResponseEntity.ok().build();
    }

    private static final DtoBuilder userDto = new DtoBuilder()
            .add("id")
            .add("firstName")
            .add("lastName")
            .add("email")
            ;
}
