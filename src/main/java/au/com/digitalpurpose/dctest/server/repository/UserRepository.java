package au.com.digitalpurpose.dctest.server.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import au.com.digitalpurpose.dctest.server.model.User;
import au.com.digitalpurpose.dctest.server.model.User.Status;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
    
    @Query("select u from User u "
            + " where lower(u.email) = lower(?1) "
            + " and u.status in (?2)")
    User findByEmailWithStatus(String email,
                               List<Status> status);

    User findFirstByResetPasswordToken(String token);
    
    Optional<User> findByConfirmationToken(String confirmationToken);

    @Query("select count(1) from User u "
            + " where lower(u.email) = lower(?1)")
    Long countByEmailIgnoreCase(String email);
    
    @Query("select count(1) from User u "
            + " where lower(u.email) = lower(?1)"
            + " and u.id != ?2")
    Long countByEmailOtherId(String email,
                             Long userId);

}
