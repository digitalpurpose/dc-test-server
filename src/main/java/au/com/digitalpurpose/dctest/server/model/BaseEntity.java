package au.com.digitalpurpose.dctest.server.model;

public interface BaseEntity {
    Long getId();
    void setId(Long id);

}
