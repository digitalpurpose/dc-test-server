package au.com.digitalpurpose.dctest.server.request.user;

import javax.validation.constraints.NotNull;

public class UpdateUserRequest extends AbstractUserRequest {

    @NotNull
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
}
