package au.com.digitalpurpose.dctest.server.model;

import javax.persistence.MappedSuperclass;

import java.io.Serializable;
import java.time.OffsetDateTime;

@MappedSuperclass
public abstract class AuditableEntity implements BaseEntity, Serializable {

    private OffsetDateTime createdOn;
    private Long createdBy;
    private OffsetDateTime updatedOn;
    private Long updatedBy;

    public AuditableEntity() {
    }

    public AuditableEntity(Long createdBy) {
        this.createdOn = OffsetDateTime.now();
        this.createdBy = createdBy;
    }

    public void updated(Long updatedBy) {
        if (createdOn == null) {
            this.createdOn = OffsetDateTime.now();
            this.createdBy = updatedBy;
        }
        this.updatedOn = OffsetDateTime.now();
        this.updatedBy = updatedBy;
    }

    public OffsetDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(OffsetDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public OffsetDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(OffsetDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }
}
