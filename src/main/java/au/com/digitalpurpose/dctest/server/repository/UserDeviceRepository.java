package au.com.digitalpurpose.dctest.server.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import au.com.digitalpurpose.dctest.server.model.UserDevice;

public interface UserDeviceRepository extends JpaRepository<UserDevice, Long>, JpaSpecificationExecutor<UserDevice> {

    @Query("select ud from UserDevice ud "
            + " where ud.identifier = ?1")
    Optional<UserDevice> findByIdentifier(String userDeviceIdentifier);
}
