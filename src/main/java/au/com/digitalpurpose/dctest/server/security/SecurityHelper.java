package au.com.digitalpurpose.dctest.server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import au.com.digitalpurpose.dctest.server.exception.NoSuchObjectException;
import au.com.digitalpurpose.dctest.server.model.User.Role;
import au.com.digitalpurpose.dctest.server.model.UserAccount;

@Service
public class SecurityHelper {

    @Autowired
    public SecurityHelper() {
    }

    public UserAccount getCurrentUser() throws NoSuchObjectException {
        AuthenticatedUserAccount principal = getPrincipal();
        if (principal != null) {
            Role role = principal.getRole();
            Long userId = principal.getId();
            /*
             * TODO
            switch (role) {

                case "user":
                    userAccount = userRepository.findByIdCached(userId)
                            .orElseThrow(() -> new NoSuchObjectException(Patient.class, userId));
                    break;

                case "admin":
                    userAccount = adminRepository.findByIdCached(userId)
                            .orElseThrow(() -> new NoSuchObjectException(Admin.class, userId));
                    break;

                default:
                    throw new RuntimeException("Unsupported role: " + role);
            }

            if (userAccount instanceof UserAccount && !((UserAccount)userAccount).getStatus().equals(UserAccount.Status.active)) {
                throw new AccessDeniedException("The current user is not an active " + role + ": " + getPrincipal());
            }
            */

            return principal;
        } else {
            return null;
        }
    }

    private AuthenticatedUserAccount getPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof UsernamePasswordAuthenticationToken) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof AuthenticatedUserAccount) {
                AuthenticatedUserAccount account = (AuthenticatedUserAccount) principal;
                return account;
            }
        }
        return null;
    }

}
