package au.com.digitalpurpose.dctest.server.exception;

public class NoSuchObjectException extends ServiceException {

    private static final String ERR = "no-such-object";

    public NoSuchObjectException(Class type, Object id) {
      super(ERR, "We could not find a " + type.getSimpleName() + " with ID " + id);
    }

    public NoSuchObjectException(String message) {
      super(ERR, message);
    }

    public NoSuchObjectException(String message, Throwable cause) {
      super(ERR, message, cause);
    }

}
