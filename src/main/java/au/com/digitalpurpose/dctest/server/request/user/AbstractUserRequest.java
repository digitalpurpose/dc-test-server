package au.com.digitalpurpose.dctest.server.request.user;

import au.com.digitalpurpose.dctest.server.model.User.Role;
import au.com.digitalpurpose.dctest.server.model.User.Status;

public abstract class AbstractUserRequest {

    private Status status;
    private String email;
    private String firstName;
    private String lastName;
    private Role role;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
    
}
