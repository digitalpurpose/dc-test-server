package au.com.digitalpurpose.dctest.server.api.admin;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import au.com.digitalpurpose.dctest.server.exception.NoSuchObjectException;
import au.com.digitalpurpose.dctest.server.model.UserAccount;
import au.com.digitalpurpose.dctest.server.model.UserDevice;
import au.com.digitalpurpose.dctest.server.request.user.SearchUserDevicesRequest;
import au.com.digitalpurpose.dctest.server.security.Caller;
import au.com.digitalpurpose.dctest.server.service.UserDeviceService;
import au.com.digitalpurpose.dctest.server.util.dto.DtoBuilder;

@RestController
@RequestMapping("/api/admin/user-device")
public class UserDeviceAdminApi {

    private final UserDeviceService userDeviceService;
    
    @Autowired
    public UserDeviceAdminApi(UserDeviceService userDeviceService) {
        this.userDeviceService = userDeviceService;
    }

    @PostMapping("search")
    public Map<String, Object> filterUsers(@RequestBody SearchUserDevicesRequest request,
                                           @Caller UserAccount caller) {
        Page<UserDevice> users = userDeviceService.filter(request);
        return userDeviceDto.buildPage(users);
    }

    @GetMapping("{identifier}")
    public Map<String, Object> getUser(@PathVariable String identifier) throws NoSuchObjectException {
        return userDeviceBriefDto.build(userDeviceService.get(identifier));
    }

    private static final DtoBuilder userDeviceBriefDto = new DtoBuilder()
            .add("id")
            .add("identifier")
            .add("createdDate")
            ;
    
    private static final DtoBuilder userDeviceDto = new DtoBuilder(userDeviceBriefDto)
            .add("totalMfd")
            .add("averageMfdPerWeek")
            ;

}
