package au.com.digitalpurpose.dctest.server.exception;

public class EmailTakenException extends AuthenticationException {

    private static final String ERR = "email_taken";

    public EmailTakenException() {
        super(ERR);
    }
}
