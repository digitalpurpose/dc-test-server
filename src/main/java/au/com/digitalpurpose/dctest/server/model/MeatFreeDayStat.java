package au.com.digitalpurpose.dctest.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.Immutable;


@Entity
@Immutable
@Table(name = "mfd_stat")
public class MeatFreeDayStat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
    
    private Long userDeviceId;
    @Column(name = "avg_week")
    private Double averagePerWeek;
    private Long numWeeks;
    private Long total;

    public Long getUserDeviceId() {
        return userDeviceId;
    }

    public void setUserDeviceId(Long userDeviceId) {
        this.userDeviceId = userDeviceId;
    }

    public Double getAveragePerWeek() {
        return averagePerWeek;
    }

    public void setAveragePerWeek(Double averagePerWeek) {
        this.averagePerWeek = averagePerWeek;
    }

    public Long getNumWeeks() {
        return numWeeks;
    }

    public void setNumWeeks(Long numWeeks) {
        this.numWeeks = numWeeks;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "MfdStat [userDeviceId=" + userDeviceId + ", averagePerWeek=" + averagePerWeek + ", numWeeks=" + numWeeks
                + ", total=" + total + "]";
    }

}
