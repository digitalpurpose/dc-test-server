package au.com.digitalpurpose.dctest.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import au.com.digitalpurpose.dctest.server.security.JwtAuthenticationEntryPoint;
import au.com.digitalpurpose.dctest.server.security.JwtAuthorizationFilter;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class DcTestSecurityConfig extends WebSecurityConfigurerAdapter {
    
    private static final Logger log = LoggerFactory.getLogger(DcTestSecurityConfig.class);

    @Value("${app.cors.urls}")
    private String allowedCorsUrls;

    @Value("${app.jwt.secret}")
    private String jwtSecret;

    @Value("${app.jwt.clock-skew}")
    private Long clockSkew;

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .cors()
        .and()
            .csrf()
                .disable()
            .authorizeRequests()
                .antMatchers("/actuator/**"
                             )
                    .permitAll()
                .antMatchers(
                        "/api/portal/auth/**",
                        "/api/admin/auth/**",
                        "/api/mobile/auth/**",
                        // for now all mobile endpoints are open
                        "/api/mobile/**",
                        "/webhook/**"
                        )
                    .permitAll()
                .antMatchers("/api/portal/**").hasRole("USER")
                .antMatchers("/api/admin/**").hasRole("ADMIN")
                .anyRequest()
                    .authenticated()
            .and()
                .addFilter(new JwtAuthorizationFilter(authenticationManager(), jwtSecret, clockSkew))
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
                .exceptionHandling()
                    .authenticationEntryPoint(jwtAuthenticationEntryPoint)
            ;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration defaultApiConfig = getDefaultApiConfiguration();
        source.registerCorsConfiguration("/api/portal/**", defaultApiConfig);
        source.registerCorsConfiguration("/api/admin/**", defaultApiConfig);
        source.registerCorsConfiguration("/api/mobile/**", getDefaultMobileApiConfiguration());
        return source;
    }

    private CorsConfiguration getDefaultApiConfiguration() {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        List<String> corsUrls = new ArrayList<>();
        log.info("Allowed CORS URLs are: {}", this.allowedCorsUrls);
        if (StringUtils.isNotBlank(allowedCorsUrls)) {
            Collections.addAll(corsUrls, allowedCorsUrls.split(","));
        }
        config.setAllowedOrigins(corsUrls);
        config.setAllowedMethods(Collections.singletonList("*"));
        config.setAllowedHeaders(Collections.singletonList("*"));
        return config;
    }
    
    private CorsConfiguration getDefaultMobileApiConfiguration() {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        List<String> corsUrls = new ArrayList<>();
        corsUrls.add("*");
        log.info("Allowing all CORS URLs for mobile");
        config.setAllowedOrigins(corsUrls);
        config.setAllowedMethods(Collections.singletonList("*"));
        config.setAllowedHeaders(Collections.singletonList("*"));
        return config;
    }

}
