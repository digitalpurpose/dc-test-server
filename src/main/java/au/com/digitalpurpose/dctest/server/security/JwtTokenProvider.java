package au.com.digitalpurpose.dctest.server.security;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import au.com.digitalpurpose.dctest.server.model.UserAccount;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

@Component
public class JwtTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);
    
    // JWT token defaults
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "dctest";
    public static final String TOKEN_AUDIENCE = "dctest-app";
    
    public static final String CLAIM_ROLE = "role";
    public static final String CLAIM_USERNAME = "username";

    private final String jwtSecret;
    private final long tokenTimeout;
    private final long refreshTimeout;
    private final long clockSkew;

    
    public JwtTokenProvider(@Value("${app.jwt.secret}") String jwtSecret,
                            @Value("${app.jwt.token-timeout}") long tokenTimeout,
                            @Value("${app.jwt.refresh-timeout}") long refreshTimeout,
                            @Value("${app.jwt.clock-skew}") long clockSkew) {
        this.jwtSecret = jwtSecret;
        this.tokenTimeout = tokenTimeout;
        this.refreshTimeout = refreshTimeout;
        this.clockSkew = clockSkew;
    }

    public String generateToken(UserAccount account) {
        return generateToken(account.getId(), account.getRole().name(), account.getUsername());
    }

    public String generateToken(Long id,
                                String role,
                                String username) {

        return Jwts.builder()
                .signWith(Keys.hmacShaKeyFor(jwtSecret.getBytes()), SignatureAlgorithm.HS512)
                .setHeaderParam("typ", TOKEN_TYPE)
                .setIssuer(TOKEN_ISSUER)
                .setAudience(TOKEN_AUDIENCE)
                .setSubject("" + id)
                .setExpiration(new Date(System.currentTimeMillis() + tokenTimeout))
                .claim(CLAIM_ROLE, role)
                .claim(CLAIM_USERNAME, username)
                .claim("scope", "bearer")
                .compact();
    }

    public String generateRefreshToken(UserAccount account) {
        return generateRefreshToken(account.getId(), account.getRole().name(), account.getUsername());
    }
    
    public String generateRefreshToken(Long id,
                                       String role,
                                       String username) {

        // TODO: store refresh token in database so they can be revoked outside the users app
        // only relevant, if refresh tokens are long-lived

        Instant now = Instant.now();
        return Jwts.builder()
                .signWith(Keys.hmacShaKeyFor(jwtSecret.getBytes()), SignatureAlgorithm.HS512)
                .setHeaderParam("typ", TOKEN_TYPE)
                .setIssuer(TOKEN_ISSUER)
                .setAudience(TOKEN_AUDIENCE)
                .setId(UUID.randomUUID().toString())
                .setSubject("" + id)
                .setIssuedAt(new Date(now.toEpochMilli()))
                .setExpiration(new Date(now.plusMillis(refreshTimeout).toEpochMilli()))
                .claim(CLAIM_ROLE, role)
                .claim(CLAIM_USERNAME, username)
                .claim("scope", "refresh_token")
                .compact();
    }

    public Token refreshToken(String refreshToken) {
        try {
            JwtParser parser = Jwts.parserBuilder()
                    .setSigningKey(jwtSecret.getBytes())
                    .setAllowedClockSkewSeconds(clockSkew)
                    .build();
            Jws<Claims> parsedToken = parser.parseClaimsJws(refreshToken);
            String idStr = parsedToken.getBody().getSubject();
            Long userId = Long.parseLong(idStr);
            String role = (String) parsedToken.getBody().get("role");
            String username = (String) parsedToken.getBody().get("username");

            // TODO: check database for refresh token entry and its expiry
            // only relevant, if refresh tokens are long-lived

            // for now just check tokens expiry directly
            Date expiryDate = parsedToken.getBody().getExpiration();
            if (expiryDate.before(new Date())) {
                throw new AccessDeniedException("refresh token expired");
            }

            String token = generateToken(userId, role, username);
            refreshToken = generateRefreshToken(userId, role, username);
            return new Token(token, refreshToken);
        } catch (Exception e) {
            throw new AccessDeniedException("refresh token has expired: " + e.getMessage());
        }
    }
}
