package au.com.digitalpurpose.dctest.server.security;

public class Token {
    private String token;
    private String refreshToken;

    public Token(String token,
                 String refreshToken) {
        this.token = token;
        this.refreshToken = refreshToken;
    }

    public String getToken() {
        return token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

}
