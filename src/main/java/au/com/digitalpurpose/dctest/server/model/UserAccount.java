package au.com.digitalpurpose.dctest.server.model;

import au.com.digitalpurpose.dctest.server.model.User.Role;
import au.com.digitalpurpose.dctest.server.model.User.Status;

public interface UserAccount {

    Long getId();
    void setId(Long id);

    String getUsername();
    void setUsername(String username);
    
    String getEmail();
    void setEmail(String email);
    
    Role getRole();
    void setRole(Role role);

    Status getStatus();
    void setStatus(Status status);

}
