package au.com.digitalpurpose.dctest.server.exception;

public class AuthenticationException extends Exception {
    
    private String errorCode;

    public AuthenticationException(String errorCode) {
        this.errorCode = errorCode;
    }

    public AuthenticationException(String errorCode,
                                   String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public AuthenticationException(String errorCode,
                                   String message,
                                   Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

}
