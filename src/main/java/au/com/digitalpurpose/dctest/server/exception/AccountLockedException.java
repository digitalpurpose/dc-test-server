package au.com.digitalpurpose.dctest.server.exception;

public class AccountLockedException extends AuthenticationException {

    private static final String ERR = "account_locked";

    public AccountLockedException() {
        super(ERR);
    }
}
