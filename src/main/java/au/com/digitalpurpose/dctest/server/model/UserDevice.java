package au.com.digitalpurpose.dctest.server.model;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.temporal.TemporalUnit;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "user_device")
@SequenceGenerator(name = "user_device_seq_gen", sequenceName = "user_device_id_seq", allocationSize = 1)
public class UserDevice {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_device_seq_gen")
    private Long id;
    private String identifier;
    private OffsetDateTime createdDate;

    @Transient
    private Long totalMfd;
    @Transient
    private Double averageMfdPerWeek;

    public UserDevice() {
    }

    public UserDevice(String identifier) {
        this.identifier = identifier;
        // For demonstration purposes, set the user back 7 days
        this.createdDate = OffsetDateTime.now().minus(Duration.ofDays(7));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Long getTotalMfd() {
        return totalMfd;
    }

    public void setTotalMfd(Long totalMfd) {
        this.totalMfd = totalMfd;
    }

    public Double getAverageMfdPerWeek() {
        return averageMfdPerWeek;
    }

    public void setAverageMfdPerWeek(Double averageMfdPerWeek) {
        this.averageMfdPerWeek = averageMfdPerWeek;
    }

    @Override
    public String toString() {
        return "UserDevice [id=" + id + ", identifier=" + identifier + ", createdDate=" + createdDate + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((identifier == null) ? 0 : identifier.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserDevice other = (UserDevice) obj;
        if (createdDate == null) {
            if (other.createdDate != null)
                return false;
        } else if (!createdDate.equals(other.createdDate))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (identifier == null) {
            if (other.identifier != null)
                return false;
        } else if (!identifier.equals(other.identifier))
            return false;
        return true;
    }

}
