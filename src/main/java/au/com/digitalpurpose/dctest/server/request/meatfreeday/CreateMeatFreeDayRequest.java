package au.com.digitalpurpose.dctest.server.request.meatfreeday;

import java.time.OffsetDateTime;

public class CreateMeatFreeDayRequest {
    private OffsetDateTime date;

    public OffsetDateTime getDate() {
        return date;
    }

    public void setDate(OffsetDateTime date) {
        this.date = date;
    }
}
