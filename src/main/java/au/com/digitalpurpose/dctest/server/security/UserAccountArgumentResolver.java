package au.com.digitalpurpose.dctest.server.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import au.com.digitalpurpose.dctest.server.model.UserAccount;

public class UserAccountArgumentResolver implements HandlerMethodArgumentResolver {



    private static final Logger log = LoggerFactory.getLogger(UserAccountArgumentResolver.class);

    private SecurityHelper securityHelper;

    @Autowired
    public UserAccountArgumentResolver(SecurityHelper securityHelper) {
        this.securityHelper = securityHelper;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(Caller.class) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        UserAccount user = securityHelper.getCurrentUser();
        /* Implement when needed
        if (parameter.getParameterType().equals(User.class) && !(user instanceof User)) {
            throw new AccessDeniedException("The current user is not an active HCP: " + user.getUsername());
        } else {
            throw new RuntimeException("Undefined caller type");
        }
        */
        return user;

    }
}
