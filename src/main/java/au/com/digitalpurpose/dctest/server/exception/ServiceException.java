package au.com.digitalpurpose.dctest.server.exception;

public class ServiceException extends Exception {

    private String errorCode;

    public ServiceException(String errorCode) {
        this.errorCode = errorCode;
    }

    public ServiceException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public ServiceException(String errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

}
