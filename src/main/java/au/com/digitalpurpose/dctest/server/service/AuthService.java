package au.com.digitalpurpose.dctest.server.service;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.digitalpurpose.dctest.server.exception.AccountLockedException;
import au.com.digitalpurpose.dctest.server.exception.AccountPendingException;
import au.com.digitalpurpose.dctest.server.exception.AuthenticationException;
import au.com.digitalpurpose.dctest.server.exception.EmailTakenException;
import au.com.digitalpurpose.dctest.server.exception.InvalidCredentialsException;
import au.com.digitalpurpose.dctest.server.exception.InvalidRequestException;
import au.com.digitalpurpose.dctest.server.exception.InvalidResetPasswordTokenException;
import au.com.digitalpurpose.dctest.server.exception.NoSuchObjectException;
import au.com.digitalpurpose.dctest.server.model.User;
import au.com.digitalpurpose.dctest.server.model.User.Status;
import au.com.digitalpurpose.dctest.server.model.UserDevice;
import au.com.digitalpurpose.dctest.server.repository.UserDeviceRepository;
import au.com.digitalpurpose.dctest.server.repository.UserRepository;
import au.com.digitalpurpose.dctest.server.request.auth.LoginRequest;
import au.com.digitalpurpose.dctest.server.request.user.ChangePasswordRequest;
import au.com.digitalpurpose.dctest.server.request.user.CheckResetForgotPasswordTokenRequest;
import au.com.digitalpurpose.dctest.server.request.user.ForgotPasswordRequest;
import au.com.digitalpurpose.dctest.server.request.user.RegisterUserDeviceRequest;
import au.com.digitalpurpose.dctest.server.request.user.RegisterUserRequest;
import au.com.digitalpurpose.dctest.server.request.user.ResetForgottenPasswordRequest;
import au.com.digitalpurpose.dctest.server.security.JwtTokenProvider;

@Service
public class AuthService {

    private static final Logger log = LoggerFactory.getLogger(AuthService.class);

    private final long maxLoginAttempts;
    private final long accountLockedPeriod;
    private final long resetForgottenPasswordTokenExpiry;

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider tokenProvider;
    
    private final UserDeviceRepository userDeviceRepository;

    public AuthService(UserRepository userRepository,
                       UserDeviceRepository userDeviceRepository,
                       PasswordEncoder passwordEncoder,
                       JwtTokenProvider tokenProvider,
                       @Value("${app.security.max-login-attempts}") long maxLoginAttempts,
                       @Value("${app.security.acount-locked-period}") long accountLockedPeriod,
                       @Value("${app.security.reset-forgot-password-token-expiry}") long resetForgottenPasswordTokenExpiry) {
        this.maxLoginAttempts = maxLoginAttempts;
        this.accountLockedPeriod = accountLockedPeriod;
        this.resetForgottenPasswordTokenExpiry = resetForgottenPasswordTokenExpiry;
        this.userRepository = userRepository;
        this.userDeviceRepository = userDeviceRepository;
        this.passwordEncoder = passwordEncoder;
        this.tokenProvider = tokenProvider;
    }

    public UserDevice registerDevice(RegisterUserDeviceRequest request) {
        
        String identifier = request.getIdentifier();
        Optional<UserDevice> userDevice = userDeviceRepository.findByIdentifier(identifier);
        if (userDevice.isPresent()) {
            // fail silently for now, if the identifier has already been registered
            log.warn("user device with ID " + identifier + " already exists");
            return userDevice.get();
        }
        
        log.info("reqistering new user device with id " + identifier);
        return userDeviceRepository.save(new UserDevice(identifier));
    }
    
    // note: we don't want the transaction to roll back, hence no rollbackFor
    // and no RuntimeException
    @Transactional
    public UserAccountWithTokens loginUser(LoginRequest request) throws AuthenticationException {

        // check for locked as well so users can be notified and given the
        // option to unlock
        List<Status> statuses = Arrays.asList(Status.active, Status.locked, Status.pending);
        User user = userRepository.findByEmailWithStatus(request.getEmail(), statuses);
        try {
            checkCanLogin(user, request);
            handleLoginSuccess(user);
        } catch (Exception e) {
            handleFailedLogin(user);
            throw e;
        }
        return createUserWithTokens(user);
    }

    @Transactional(rollbackFor = Exception.class)
    public User registerUser(RegisterUserRequest request)
            throws InvalidRequestException, EmailTakenException, NoSuchObjectException {

        // Check if the patient has already registered
        Long countEmail = userRepository.countByEmailIgnoreCase(request.getEmail());
        if (countEmail > 0) {
            throw new EmailTakenException();
        }

        // Check if the passwords match
        if (!request.getPassword().equals(request.getConfirmationPassword())) {
            throw new InvalidRequestException("Confirmation password does not match");
        }

        // Create the user
        User user = new User(request.getEmail(), passwordEncoder.encode(request.getPassword()), request.getFirstName(),
                             request.getLastName(), request.getRole(), Status.pending);
        // TODO: for now we are bypassing user confirmation, probably needs to be an SMS
        //user.setConfirmationToken(UUID.randomUUID().toString());
        user.setStatus(Status.active);
        user = userRepository.save(user);

        // TODO: Send out the registration confirmation email
        // emailHelper.sendPatientRegistrationConfirmationEmail(patient);

        return user;
    }

    public User confirmPatientRegistration(String token) throws NoSuchObjectException {
        User user = userRepository.findByConfirmationToken(token)
                .orElseThrow(() -> new NoSuchObjectException(User.class, token));
        user.setConfirmationToken(null);
        user.setStatus(Status.active);
        return userRepository.save(user);
    }

    @Transactional(rollbackFor = Exception.class)
    public User changePassword(ChangePasswordRequest request,
                               User user) {
        user.setPasswordEnc(passwordEncoder.encode(request.getPassword()));
        userRepository.save(user);
        return user;
    }

    @Transactional(rollbackFor = Exception.class)
    public void forgotPassword(ForgotPasswordRequest request) {
        List<Status> statuses = Arrays.asList(Status.active, Status.locked, Status.pending);
        User user = userRepository.findByEmailWithStatus(request.getEmail(), statuses);
        doForgotPassword(user);
    }

    @Transactional(rollbackFor = Exception.class)
    public void resetForgotPassword(ResetForgottenPasswordRequest request) throws InvalidResetPasswordTokenException {
        User userAccount = userRepository.findFirstByResetPasswordToken(request.getConfirmationToken());
        doResetForgottenPassword(request, userAccount);
    }

    @Transactional(rollbackFor = Exception.class)
    public void checkResetForgotPasswordToken(CheckResetForgotPasswordTokenRequest request)
            throws InvalidResetPasswordTokenException {
        User userAccount = userRepository.findFirstByResetPasswordToken(request.getConfirmationToken());
        doCheckResetForgottenPassword(userAccount);
    }

    private void checkCanLogin(User user,
                               LoginRequest request)
            throws InvalidCredentialsException, AccountLockedException, AccountPendingException {
        if (user == null) {
            throw new InvalidCredentialsException();
        }
        if (!passwordEncoder.matches(request.getPassword(), user.getPasswordEnc())) {
            throw new InvalidCredentialsException();
        }
        if (user.getStatus().equals(Status.pending)) {
            throw new AccountPendingException();
        }
        if (user.getStatus().equals(Status.locked)) {
            // if the account is currently locked and the locking period has not
            // expired, do not allow in
            OffsetDateTime accountLockedAt = user.getAccountLockedAt();
            if (accountLockedAt != null
                    && accountLockedAt.plusSeconds(accountLockedPeriod).isAfter(OffsetDateTime.now())) {
                throw new AccountLockedException();
            }
        }
    }

    private void handleLoginSuccess(User user) {
        user.setFirstFailedLoginAt(null);
        user.setNumFailedLogins(0);
        user.setAccountLockedAt(null);
        user.setLastLogin(OffsetDateTime.now());
        // not sure if someone with a status of invited or inactive will ever be
        // allowed to login
        if (Status.locked.equals(user.getStatus())) {
            user.setStatus(Status.active);
        }
        userRepository.save(user);
    }

    private void handleFailedLogin(User user) throws AccountLockedException {
        if (user != null) {
            int numFailedLogins = (user.getNumFailedLogins() != null ? user.getNumFailedLogins() : 0) + 1;
            user.setNumFailedLogins(numFailedLogins);

            if (numFailedLogins == 1) {
                user.setFirstFailedLoginAt(OffsetDateTime.now());
                userRepository.save(user);
            } else if (numFailedLogins >= maxLoginAttempts) {
                user.setStatus(Status.locked);
                user.setAccountLockedAt(OffsetDateTime.now());
                userRepository.save(user);
                // Explicitly throw the exception to avoid the incorrect
                // exception propagating to the client
                throw new AccountLockedException();
            }
        }
    }

    private UserAccountWithTokens createUserWithTokens(User user) {
        String token = tokenProvider.generateToken(user);
        String refreshToken = tokenProvider.generateRefreshToken(user);
        return new UserAccountWithTokens(user, token, refreshToken);
    }

    protected void doForgotPassword(User user) {
        user.setResetPasswordToken(UUID.randomUUID().toString());
        user.setResetPasswordTokenExpiry(OffsetDateTime.now().plusMinutes(resetForgottenPasswordTokenExpiry));
        userRepository.save(user);
        // TODO: send email
        // emailHelper.sendResetForgottenPasswordEmail(user);
    }

    protected void doCheckResetForgottenPassword(User user) throws InvalidResetPasswordTokenException {
        if (user == null || !user.getResetPasswordTokenExpiry().isAfter(OffsetDateTime.now())) {
            throw new InvalidResetPasswordTokenException();
        }
    }

    protected void doResetForgottenPassword(ResetForgottenPasswordRequest request,
                                            User user)
            throws InvalidResetPasswordTokenException {
        doCheckResetForgottenPassword(user);
        user.setResetPasswordToken(null);
        user.setResetPasswordTokenExpiry(null);
        changePassword(request, user);
    }

    // -------------------------------------------------------------------------

    public static final class UserAccountWithTokens {
        private String token;
        private String refreshToken;
        private User user;

        public UserAccountWithTokens(User user,
                                     String token,
                                     String refreshToken) {
            this.token = token;
            this.refreshToken = refreshToken;
            this.user = user;
        }

        public String getToken() {
            return token;
        }

        public String getRefreshToken() {
            return refreshToken;
        }

        public User getUser() {
            return user;
        }
    }

}
