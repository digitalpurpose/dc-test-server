package au.com.digitalpurpose.dctest.server.model;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "mfd")
@SequenceGenerator(name = "mfd_seq_gen", sequenceName = "mfd_id_seq", allocationSize = 1)
public class MeatFreeDay {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mfd_seq_gen")
    private Long id;
    
    private OffsetDateTime mfdDate;

    @Column(name = "user_device_id", insertable = false, updatable = false)
    private Long userDeviceId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_device_id")
    private UserDevice userDevice;

    public MeatFreeDay() {
    }

    public MeatFreeDay(OffsetDateTime mfdDate,
                       UserDevice userDevice) {
        this.mfdDate = mfdDate;
        this.userDevice = userDevice;
        if (userDevice != null) {
            this.userDeviceId = userDevice.getId();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OffsetDateTime getMfdDate() {
        return mfdDate;
    }

    public void setMfdDate(OffsetDateTime mfdDate) {
        this.mfdDate = mfdDate;
    }

    public Long getUserDeviceId() {
        return userDeviceId;
    }

    public void setUserDeviceId(Long userDeviceId) {
        this.userDeviceId = userDeviceId;
    }

    public UserDevice getUserDevice() {
        return userDevice;
    }

    public void setUserDevice(UserDevice userDevice) {
        this.userDevice = userDevice;
    }

    @Override
    public String toString() {
        return "MeatFreeDay [id=" + id + ", mfdDate=" + mfdDate + ", userDeviceId=" + userDeviceId + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((mfdDate == null) ? 0 : mfdDate.hashCode());
        result = prime * result + ((userDeviceId == null) ? 0 : userDeviceId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MeatFreeDay other = (MeatFreeDay) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (mfdDate == null) {
            if (other.mfdDate != null)
                return false;
        } else if (!mfdDate.equals(other.mfdDate))
            return false;
        if (userDeviceId == null) {
            if (other.userDeviceId != null)
                return false;
        } else if (!userDeviceId.equals(other.userDeviceId))
            return false;
        return true;
    }

}
