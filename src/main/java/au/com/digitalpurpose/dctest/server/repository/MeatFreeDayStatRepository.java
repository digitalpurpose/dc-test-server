package au.com.digitalpurpose.dctest.server.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import au.com.digitalpurpose.dctest.server.model.MeatFreeDay;
import au.com.digitalpurpose.dctest.server.model.MeatFreeDayStat;

public interface MeatFreeDayStatRepository extends JpaRepository<MeatFreeDay, Long>, JpaSpecificationExecutor<MeatFreeDay> {

    @Query("select m from MeatFreeDayStat m "
            + " where m.userDeviceId in (?1)")
    List<MeatFreeDayStat> findForDeviceIds(Collection<Long> ids);
}
