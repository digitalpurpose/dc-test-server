package au.com.digitalpurpose.dctest.server.security;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import au.com.digitalpurpose.dctest.server.model.User.Role;
import au.com.digitalpurpose.dctest.server.model.User.Status;
import au.com.digitalpurpose.dctest.server.model.UserAccount;

public class AuthenticatedUserAccount extends AbstractAuthenticationToken implements UserAccount {

    private static final long serialVersionUID = -1226778621704979144L;
    
    private Long id;
    private String email;
    private Role role;
    private Status status;

    public AuthenticatedUserAccount(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    public AuthenticatedUserAccount(Collection<? extends GrantedAuthority> authorities,
                                    Long id,
                                    String email,
                                    Role role,
                                    Status status) {
        super(authorities);
        this.id = id;
        this.email = email;
        this.role = role;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public void setUsername(String username) {
        this.email = username;
    }

    @Override
    public Object getCredentials() {
        return getUsername();
    }

    @Override
    public Object getPrincipal() {
        return getId();
    }

}
