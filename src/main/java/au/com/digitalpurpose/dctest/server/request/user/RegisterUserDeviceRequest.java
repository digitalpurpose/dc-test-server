package au.com.digitalpurpose.dctest.server.request.user;

import javax.validation.constraints.NotBlank;

public class RegisterUserDeviceRequest {
    
    @NotBlank
    private String identifier;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    
}
