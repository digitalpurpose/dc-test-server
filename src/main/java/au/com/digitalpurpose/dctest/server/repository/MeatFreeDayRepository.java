package au.com.digitalpurpose.dctest.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import au.com.digitalpurpose.dctest.server.model.MeatFreeDay;

public interface MeatFreeDayRepository extends JpaRepository<MeatFreeDay, Long>, JpaSpecificationExecutor<MeatFreeDay> {

    @Query("select m from MeatFreeDay m "
            + " join m.userDevice u "
            + " where u.identifier = ?1 "
            + " order by m.mfdDate asc")
    List<MeatFreeDay> findForUser(String userDeviceIdentifier);
}
