package au.com.digitalpurpose.dctest.server.api.portal;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import au.com.digitalpurpose.dctest.server.exception.AuthenticationException;
import au.com.digitalpurpose.dctest.server.exception.EmailTakenException;
import au.com.digitalpurpose.dctest.server.exception.InvalidRequestException;
import au.com.digitalpurpose.dctest.server.exception.InvalidResetPasswordTokenException;
import au.com.digitalpurpose.dctest.server.exception.NoSuchObjectException;
import au.com.digitalpurpose.dctest.server.request.auth.LoginRequest;
import au.com.digitalpurpose.dctest.server.request.user.CheckResetForgotPasswordTokenRequest;
import au.com.digitalpurpose.dctest.server.request.user.ForgotPasswordRequest;
import au.com.digitalpurpose.dctest.server.request.user.RegisterUserRequest;
import au.com.digitalpurpose.dctest.server.request.user.ResetForgottenPasswordRequest;
import au.com.digitalpurpose.dctest.server.security.JwtTokenProvider;
import au.com.digitalpurpose.dctest.server.security.Token;
import au.com.digitalpurpose.dctest.server.service.AuthService;
import au.com.digitalpurpose.dctest.server.service.AuthService.UserAccountWithTokens;
import au.com.digitalpurpose.dctest.server.util.dto.DtoBuilder;

@RestController
@RequestMapping("/api/portal/auth")
public class AuthPortalApi {

    private final AuthService authService;
    private final JwtTokenProvider jwtTokenProvider;

    @Autowired
    public AuthPortalApi(AuthService authService,
                         JwtTokenProvider jwtTokenProvider) {
        this.authService = authService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping
    public Map<String, Object> login(@RequestBody LoginRequest request)
            throws AuthenticationException {
        UserAccountWithTokens auth = authService.loginUser(request);
        Map<String, Object> result = new HashMap<>();
        result.put("token", auth.getToken());
        result.put("refresh_token", auth.getRefreshToken());
        result.put("user", userDto.build(auth.getUser()));
        return result;
    }

    @PostMapping("keepalive")
    public Map<String, Object> keepAlive(@RequestBody LoginRequest request, HttpServletRequest servletRequest)
            throws AuthenticationException {
        UserAccountWithTokens auth = authService.loginUser(request);
        Map<String, Object> result = new HashMap<>();
        result.put("token", auth.getToken());
        result.put("refresh_token", auth.getRefreshToken());
        result.put("user", userDto.build(auth.getUser()));
        return result;
    }

    @PostMapping("refresh")
    public Map<String, Object> refreshToken(@RequestBody String refreshToken) {
        Token token = jwtTokenProvider.refreshToken(refreshToken);
        Map<String, Object> result = new HashMap<>();
        result.put("token", token.getToken());
        result.put("refresh_token", token.getRefreshToken());
        return result;
    }

    @PostMapping("register")
    public ResponseEntity<Object> register(@Valid @RequestBody RegisterUserRequest request)
            throws InvalidRequestException, EmailTakenException, NoSuchObjectException {
        authService.registerUser(request);
        return ResponseEntity.ok().build();
    }

    @PostMapping("forgot-password")
    public ResponseEntity<Object> forgotPassword(@Valid @RequestBody ForgotPasswordRequest request) {
        authService.forgotPassword(request);
        return ResponseEntity.ok().build();
    }

    @PostMapping("reset-forgot-password")
    public ResponseEntity<Object> resetForgottenPassword(@Valid @RequestBody ResetForgottenPasswordRequest request)
            throws InvalidResetPasswordTokenException {
        authService.resetForgotPassword(request);
        return ResponseEntity.ok().build();
    }

    @PostMapping("reset-forgot-password/check")
    public ResponseEntity<Object> checkResetForgottenPasswordToken(@Valid @RequestBody CheckResetForgotPasswordTokenRequest request)
            throws InvalidResetPasswordTokenException {
        authService.checkResetForgotPasswordToken(request);
        return ResponseEntity.ok().build();
    }

    private static final DtoBuilder userDto = new DtoBuilder()
            .add("id")
            .add("firstName")
            .add("lastName")
            .add("email")
            ;

}
