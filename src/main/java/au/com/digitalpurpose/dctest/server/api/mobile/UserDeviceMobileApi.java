package au.com.digitalpurpose.dctest.server.api.mobile;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import au.com.digitalpurpose.dctest.server.exception.NoSuchObjectException;
import au.com.digitalpurpose.dctest.server.service.UserDeviceService;
import au.com.digitalpurpose.dctest.server.util.dto.DtoBuilder;

@RestController
@RequestMapping("/api/mobile/user-device")
public class UserDeviceMobileApi {

    private final UserDeviceService userDeviceService;
    
    @Autowired
    public UserDeviceMobileApi(UserDeviceService userDeviceService) {
        this.userDeviceService = userDeviceService;
    }

    @GetMapping("{identifier}")
    public Map<String, Object> getUser(@PathVariable String identifier) throws NoSuchObjectException {
        return userDeviceBriefDto.build(userDeviceService.get(identifier));
    }

    private static final DtoBuilder userDeviceBriefDto = new DtoBuilder()
            .add("id")
            .add("identifier")
            .add("createdDate")
            ;

}
