package au.com.digitalpurpose.dctest.server.request.user;

import au.com.digitalpurpose.dctest.server.model.User.Role;
import au.com.digitalpurpose.dctest.server.model.User.Status;
import au.com.digitalpurpose.dctest.server.request.SearchRequest;

public class SearchUsersRequest extends SearchRequest {

    private String term;
    private Status status;
    private Role role;

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
    
}
