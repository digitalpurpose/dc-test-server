package au.com.digitalpurpose.dctest.server.model;

import java.time.OffsetDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "users")
@SequenceGenerator(name = "user_seq_gen", sequenceName = "users_id_seq", allocationSize = 1)
public class User extends AuditableEntity implements UserAccount {

    private static final long serialVersionUID = 1679606778549627557L;

    public enum Status {
        pending, 
        active, 
        locked, 
        inactive, 
        deleted,
    }

    public enum Role {
        user, 
        admin,
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq_gen")
    private Long id;

    private String email;
    private String passwordEnc;
    private String firstName;
    private String lastName;
    @Enumerated(EnumType.STRING)
    private Role role;
    @Enumerated(EnumType.STRING)
    private Status status;
    private String confirmationToken;

    private OffsetDateTime lastLogin;
    private OffsetDateTime lastPasswordUpdate;
    private OffsetDateTime firstFailedLoginAt;
    private OffsetDateTime accountLockedAt;
    private Integer numFailedLogins;

    private String resetPasswordToken;
    private OffsetDateTime resetPasswordTokenExpiry;

    public User() {
    }

    public User(String email,
                String passwordEnc,
                String firstName,
                String lastName,
                Role role,
                Status status) {
        this.email = email;
        this.passwordEnc = passwordEnc;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return email;
    }

    public void setUsername(String username) {
        this.email = username;
    }

    public String getPasswordEnc() {
        return passwordEnc;
    }

    public void setPasswordEnc(String passwordEnc) {
        this.passwordEnc = passwordEnc;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public OffsetDateTime getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(OffsetDateTime lastLogin) {
        this.lastLogin = lastLogin;
    }

    public OffsetDateTime getLastPasswordUpdate() {
        return lastPasswordUpdate;
    }

    public void setLastPasswordUpdate(OffsetDateTime lastPasswordUpdate) {
        this.lastPasswordUpdate = lastPasswordUpdate;
    }

    public OffsetDateTime getFirstFailedLoginAt() {
        return firstFailedLoginAt;
    }

    public void setFirstFailedLoginAt(OffsetDateTime firstFailedLoginAt) {
        this.firstFailedLoginAt = firstFailedLoginAt;
    }

    public OffsetDateTime getAccountLockedAt() {
        return accountLockedAt;
    }

    public void setAccountLockedAt(OffsetDateTime accountLockedAt) {
        this.accountLockedAt = accountLockedAt;
    }

    public Integer getNumFailedLogins() {
        return numFailedLogins;
    }

    public void setNumFailedLogins(Integer numFailedLogins) {
        this.numFailedLogins = numFailedLogins;
    }

    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    public OffsetDateTime getResetPasswordTokenExpiry() {
        return resetPasswordTokenExpiry;
    }

    public void setResetPasswordTokenExpiry(OffsetDateTime resetPasswordTokenExpiry) {
        this.resetPasswordTokenExpiry = resetPasswordTokenExpiry;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", email=" + email + ", firstName=" + firstName + ", lastName=" + lastName + ", role="
                + role + ", status=" + status + ", lastLogin=" + lastLogin + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((lastLogin == null) ? 0 : lastLogin.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((role == null) ? 0 : role.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (email == null) {
            if (other.email != null)
                return false;
        } else if (!email.equals(other.email))
            return false;
        if (firstName == null) {
            if (other.firstName != null)
                return false;
        } else if (!firstName.equals(other.firstName))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (lastLogin == null) {
            if (other.lastLogin != null)
                return false;
        } else if (!lastLogin.equals(other.lastLogin))
            return false;
        if (lastName == null) {
            if (other.lastName != null)
                return false;
        } else if (!lastName.equals(other.lastName))
            return false;
        if (role != other.role)
            return false;
        if (status != other.status)
            return false;
        return true;
    }

}
