package au.com.digitalpurpose.dctest.server.exception;

public class InvalidCredentialsException extends AuthenticationException {

    private static final String ERR = "invalid_credentials";

    public InvalidCredentialsException() {
        super(ERR);
    }
}
