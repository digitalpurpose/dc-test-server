package au.com.digitalpurpose.dctest.server.request.user;

import javax.validation.constraints.NotBlank;

public class ResetForgottenPasswordRequest extends ChangePasswordRequest {

    @NotBlank
    private String confirmationToken;

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }
}
