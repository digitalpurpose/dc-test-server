package au.com.digitalpurpose.dctest.server;

import java.time.ZoneId;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAsync
@EnableCaching
public class DcTestServerApplication {

    private static final Logger log = LoggerFactory.getLogger(DcTestServerApplication.class);
    
    public DcTestServerApplication() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        log.info("=========================== STARTING ================================");
        log.info("System is running in timezone " + ZoneId.systemDefault());
    }

    public static void main(String[] args) {
      SpringApplication.run(DcTestServerApplication.class, args);
    }

}
