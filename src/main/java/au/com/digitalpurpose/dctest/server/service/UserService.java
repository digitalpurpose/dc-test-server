package au.com.digitalpurpose.dctest.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.digitalpurpose.dctest.server.exception.EmailTakenException;
import au.com.digitalpurpose.dctest.server.exception.InvalidRequestException;
import au.com.digitalpurpose.dctest.server.exception.NoSuchObjectException;
import au.com.digitalpurpose.dctest.server.exception.ServiceException;
import au.com.digitalpurpose.dctest.server.model.User;
import au.com.digitalpurpose.dctest.server.model.User.Status;
import au.com.digitalpurpose.dctest.server.model.UserAccount;
import au.com.digitalpurpose.dctest.server.repository.UserRepository;
import au.com.digitalpurpose.dctest.server.repository.UserSpecification;
import au.com.digitalpurpose.dctest.server.request.user.CreateUserRequest;
import au.com.digitalpurpose.dctest.server.request.user.SearchUsersRequest;
import au.com.digitalpurpose.dctest.server.request.user.UpdatePasswordRequest;
import au.com.digitalpurpose.dctest.server.request.user.UpdateUserRequest;

@Service
public class UserService {
    
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository,
                       PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public Page<User> filter(SearchUsersRequest request) {
        return userRepository.findAll(UserSpecification.filter(request), request.getPageRequest());
    }

    public User get(Long id) throws NoSuchObjectException {
        return userRepository.findById(id).orElseThrow(() -> new NoSuchObjectException(User.class, id));
    }

    @Transactional(rollbackFor = Exception.class)
    public User create(CreateUserRequest request, 
                       UserAccount caller) throws ServiceException, EmailTakenException {

        // check that email does not already exist
        Long countEmail = userRepository.countByEmailIgnoreCase(request.getEmail());
        if (countEmail > 0) {
            throw new EmailTakenException();
        }

        try {
            User user = new User(request.getEmail(), passwordEncoder.encode(request.getPassword()), request.getFirstName(),
                                 request.getLastName(), request.getRole(), Status.pending);
            user.updated(caller.getId());

            // TODO: send welcome and email verification email etc

            return userRepository.save(user);

        } catch (Exception e) {
            throw new ServiceException("create_user_failed", e.getMessage());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public User update(UpdateUserRequest request, 
                       UserAccount caller) throws NoSuchObjectException, EmailTakenException {
        User user = get(request.getId());

        // check that email does not already exist on another user
        Long countEmail = userRepository.countByEmailOtherId(request.getEmail(), user.getId());
        if (countEmail > 0) {
            throw new EmailTakenException();
        }

        user.setStatus(request.getStatus());
        user.setEmail(request.getEmail());
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.updated(caller.getId());
        return userRepository.save(user);
    }

    @Transactional(rollbackFor = Exception.class)
    public User updatePassword(UpdatePasswordRequest request, 
                               UserAccount caller) throws ServiceException {
        if (!request.getPassword().equals(request.getConfirmationPassword())) {
            throw new InvalidRequestException("Confirmation password does not match");
        }
        User user = get(request.getId());
        try {
            user.setPasswordEnc(passwordEncoder.encode(request.getPassword()));
        } catch (Exception e) {
            throw new ServiceException("update_admin_password_failed", e.getMessage());
        }
        user.updated(caller.getId());
        return userRepository.save(user);
    }

    public User changePassword(UpdatePasswordRequest request, 
                               UserAccount caller) throws ServiceException {
        if (!request.getPassword().equals(request.getConfirmationPassword())) {
            throw new InvalidRequestException("Confirmation password does not match");
        }

        User user = get(caller.getId());
        
        if (!passwordEncoder.matches(request.getOldPassword(), user.getPasswordEnc())) {
            throw new InvalidRequestException("Invalid request");
        }

        String newPassword = passwordEncoder.encode(request.getPassword());
        user.setPasswordEnc(newPassword);
        user.updated(caller.getId());
        return userRepository.save(user);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id, 
                       UserAccount caller) throws NoSuchObjectException {
        User user = get(id);
        user.setStatus(Status.deleted);
        user.updated(caller.getId());
        userRepository.save(user);
    }

    public User unlock(Long id, 
                       UserAccount caller) throws NoSuchObjectException {
        User user = get(id);
        user.setStatus(Status.active);
        user.setFirstFailedLoginAt(null);
        user.setNumFailedLogins(0);
        user.setAccountLockedAt(null);
        user.updated(caller.getId());
        return userRepository.save(user);
    }

}
