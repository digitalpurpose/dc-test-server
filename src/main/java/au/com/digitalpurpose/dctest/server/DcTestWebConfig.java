package au.com.digitalpurpose.dctest.server;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import au.com.digitalpurpose.dctest.server.security.SecurityHelper;
import au.com.digitalpurpose.dctest.server.security.UserAccountArgumentResolver;

@Configuration
public class DcTestWebConfig implements WebMvcConfigurer {

    @Autowired
    private SecurityHelper securityHelper;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new UserAccountArgumentResolver(securityHelper));
    }

}
