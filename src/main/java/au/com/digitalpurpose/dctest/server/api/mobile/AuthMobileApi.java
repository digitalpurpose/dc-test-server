package au.com.digitalpurpose.dctest.server.api.mobile;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import au.com.digitalpurpose.dctest.server.exception.EmailTakenException;
import au.com.digitalpurpose.dctest.server.exception.InvalidRequestException;
import au.com.digitalpurpose.dctest.server.exception.NoSuchObjectException;
import au.com.digitalpurpose.dctest.server.request.user.RegisterUserDeviceRequest;
import au.com.digitalpurpose.dctest.server.service.AuthService;

@RestController
@RequestMapping("/api/mobile/auth")
public class AuthMobileApi {

    private final AuthService authService;

    @Autowired
    public AuthMobileApi(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("register/device")
    public ResponseEntity<Object> registerDevice(@Valid @RequestBody RegisterUserDeviceRequest request)
            throws InvalidRequestException, EmailTakenException, NoSuchObjectException {
        // register the device when the app is first launched so we can create a unique user account
        authService.registerDevice(request);
        return ResponseEntity.ok().build();
    }

}
