package au.com.digitalpurpose.dctest.server.exception;

public class InvalidResetPasswordTokenException extends AuthenticationException {

    private static final String ERR = "invalid_reset_password_token";

    public InvalidResetPasswordTokenException() {
        super(ERR);
    }
}
