package au.com.digitalpurpose.dctest.server.api.admin;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import au.com.digitalpurpose.dctest.server.exception.EmailTakenException;
import au.com.digitalpurpose.dctest.server.exception.NoSuchObjectException;
import au.com.digitalpurpose.dctest.server.exception.ServiceException;
import au.com.digitalpurpose.dctest.server.model.User;
import au.com.digitalpurpose.dctest.server.model.UserAccount;
import au.com.digitalpurpose.dctest.server.request.user.CreateUserRequest;
import au.com.digitalpurpose.dctest.server.request.user.SearchUsersRequest;
import au.com.digitalpurpose.dctest.server.request.user.UpdatePasswordRequest;
import au.com.digitalpurpose.dctest.server.request.user.UpdateUserRequest;
import au.com.digitalpurpose.dctest.server.security.Caller;
import au.com.digitalpurpose.dctest.server.service.UserService;
import au.com.digitalpurpose.dctest.server.util.dto.DtoBuilder;

@RestController
@RequestMapping("/api/admin/user")
public class UserAdminApi {

    private final UserService userService;

    @Autowired
    public UserAdminApi(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("search")
    public Map<String, Object> filterUsers(@RequestBody SearchUsersRequest request,
                                           @Caller UserAccount caller) {
        Page<User> users = userService.filter(request);
        return userDto.buildPage(users);
    }

    @GetMapping("{id}")
    public Map<String, Object> getUser(@PathVariable Long id) throws NoSuchObjectException {
        return userDto.build(userService.get(id));
    }

    @PostMapping
    public Map<String, Object> createUser(@RequestBody CreateUserRequest request,
                                           @Caller UserAccount caller)
            throws ServiceException, EmailTakenException {
        return userDto.build(userService.create(request, caller));
    }

    @PutMapping
    public Map<String, Object> updateUser(@RequestBody UpdateUserRequest request,
                                           @Caller UserAccount caller)
            throws NoSuchObjectException, EmailTakenException {
        return userDto.build(userService.update(request, caller));
    }

    @PostMapping("change-password")
    public ResponseEntity<Object> changeUserPassword(@RequestBody UpdatePasswordRequest request,
                                                      @Caller UserAccount caller)
            throws ServiceException {
        userService.changePassword(request, caller);
        return ResponseEntity.ok().build();
    }

    @PutMapping("{id}/password")
    public Map<String, Object> updateUserPassword(@PathVariable Long id,
                                                   @RequestBody UpdatePasswordRequest request,
                                                   @Caller UserAccount caller)
            throws ServiceException {
        request.setId(id);
        return userDto.build(userService.updatePassword(request, caller));
    }

    @PutMapping("{id}/unlock")
    public Map<String, Object> unlockUser(@PathVariable Long id,
                                           @Caller UserAccount caller)
            throws NoSuchObjectException {
        return userDto.build(userService.unlock(id, caller));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long id,
                                              @Caller UserAccount caller)
            throws NoSuchObjectException {
        userService.delete(id, caller);
        return ResponseEntity.ok().build();
    }

    private static final DtoBuilder userDto = new DtoBuilder()
            .add("id")
            .add("status")
            .add("email")
            .add("firstName")
            .add("lastName")
            .add("createdOn")
            .add("lastLogin")
            .add("lastPasswordUpdate")
            .add("firstFailedLoginAt")
            .add("accountLockedAt")
            .add("numFailedLogins");

}
