package au.com.digitalpurpose.dctest.server.api.mobile;

import java.util.List;
import java.util.Map;

import au.com.digitalpurpose.dctest.server.request.meatfreeday.CreateMeatFreeDayRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import au.com.digitalpurpose.dctest.server.exception.NoSuchObjectException;
import au.com.digitalpurpose.dctest.server.model.MeatFreeDay;
import au.com.digitalpurpose.dctest.server.model.UserAccount;
import au.com.digitalpurpose.dctest.server.security.Caller;
import au.com.digitalpurpose.dctest.server.service.MeatFreeDayService;
import au.com.digitalpurpose.dctest.server.util.dto.DtoBuilder;

@RestController
@RequestMapping("/api/mobile/mfd")
public class MeatFreeDayMobileApi {

    private final MeatFreeDayService meatFreeDayService;

    @Autowired
    public MeatFreeDayMobileApi(MeatFreeDayService meatFreeDayService) {
        this.meatFreeDayService = meatFreeDayService;
    }

    @GetMapping("{identifier}")
    public List<Map<String, Object>> getMfdForUser(@PathVariable String identifier,
                                                   @Caller UserAccount caller) throws NoSuchObjectException {
        List<MeatFreeDay> mfds = meatFreeDayService.getMfdsForUser(identifier, caller);
        return mfdDto.buildList(mfds);
    }

    @PostMapping("{identifier}")
    public Map<String, Object> createMfdForUser(@PathVariable String identifier,
                                                @RequestBody CreateMeatFreeDayRequest request,
                                                @Caller UserAccount caller) throws NoSuchObjectException {
        MeatFreeDay mfd = meatFreeDayService.createMeatFreeDay(identifier, request, caller);
        return mfdDto.build(mfd);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteMfd(@PathVariable Long id,
                                            @Caller UserAccount caller) {
        meatFreeDayService.deleteMeatFreeDay(id, caller);
        return ResponseEntity.ok().build();
    }

    private static final DtoBuilder mfdDto = new DtoBuilder()
            .add("id")
            .add("mfdDate")
            ;
}
