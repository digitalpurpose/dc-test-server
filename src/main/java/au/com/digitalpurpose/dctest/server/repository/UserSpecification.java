package au.com.digitalpurpose.dctest.server.repository;

import javax.persistence.criteria.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import au.com.digitalpurpose.dctest.server.model.User;
import au.com.digitalpurpose.dctest.server.request.user.SearchUsersRequest;

public class UserSpecification {

    public static Specification<User> filter(final SearchUsersRequest request) {
        
      return (user, query, builder) -> {
        Predicate conjunction = builder.conjunction();

        if (StringUtils.isNotBlank(request.getTerm())) {
          String lowerCaseMatchTerm = request.getTerm().toLowerCase();
          String[] words = lowerCaseMatchTerm.split(" ");
          for (String word : words) {
            String likeMatchTerm = "%" + word + "%";
            conjunction.getExpressions().add(
                    builder.or(
                            builder.like(builder.lower(user.get("firstName")), likeMatchTerm),
                            builder.like(builder.lower(user.get("lastName")), likeMatchTerm),
                            builder.like(builder.lower(user.get("email")), likeMatchTerm)
                    )
            );
          }
        }

        if (request.getStatus() != null) {
          conjunction.getExpressions().add(builder.equal(user.get("status"), request.getStatus()));
        }
        
        if (request.getRole() != null) {
            conjunction.getExpressions().add(builder.equal(user.get("role"), request.getRole()));
        }

        return conjunction;
      };
    }

}
