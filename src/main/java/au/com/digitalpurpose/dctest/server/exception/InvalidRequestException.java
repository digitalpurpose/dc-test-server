package au.com.digitalpurpose.dctest.server.exception;

public class InvalidRequestException extends ServiceException {

  private static final String ERR = "invalid-request";

  public InvalidRequestException(String message) {
    super(ERR, message);
  }

  public InvalidRequestException(String message, Throwable cause) {
    super(ERR, message, cause);
  }
}
