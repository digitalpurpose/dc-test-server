package au.com.digitalpurpose.dctest.server.exception;

public class AccountPendingException extends AuthenticationException {

    private static final String ERR = "account_pending";

    public AccountPendingException() {
        super(ERR);
    }
}
