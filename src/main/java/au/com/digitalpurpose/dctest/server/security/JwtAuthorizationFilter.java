package au.com.digitalpurpose.dctest.server.security;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import au.com.digitalpurpose.dctest.server.model.User.Role;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;


public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private final String jwtSecret;
    private final Long clockSkew;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager,
                                  String jwtSecret,
                                  Long clockSkew) {
        super(authenticationManager);
        this.jwtSecret = jwtSecret;
        this.clockSkew = clockSkew;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, 
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws IOException, ServletException {
        UsernamePasswordAuthenticationToken authentication = getAuthentication(request);
        if (authentication == null) {
            filterChain.doFilter(request, response);
            return;
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(JwtTokenProvider.TOKEN_HEADER);
        if (StringUtils.isNotEmpty(token) && token.startsWith(JwtTokenProvider.TOKEN_PREFIX)) {
            byte[] signingKey = jwtSecret.getBytes();

            JwtParser parser = Jwts.parserBuilder()
                    .setSigningKey(signingKey)
                    .setAllowedClockSkewSeconds(clockSkew)
                    .build();
            Jws<Claims> parsedToken = parser.parseClaimsJws(token.replace(JwtTokenProvider.TOKEN_PREFIX, ""));

            String idStr = parsedToken.getBody().getSubject();
            Long id = Long.parseLong(idStr);
            String username = (String) parsedToken.getBody().get(JwtTokenProvider.CLAIM_USERNAME);

            String role = (String) parsedToken.getBody().get(JwtTokenProvider.CLAIM_ROLE);
            List<SimpleGrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + role.toUpperCase()));
            Role roleEnum = Role.valueOf(role);

            if (StringUtils.isNotEmpty(idStr)) {
                return new UsernamePasswordAuthenticationToken(new AuthenticatedUserAccount(authorities, id, username, roleEnum, null), null, authorities); // TODO: status
            }
        }
        return null;
    }
}
