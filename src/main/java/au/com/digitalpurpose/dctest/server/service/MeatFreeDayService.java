package au.com.digitalpurpose.dctest.server.service;

import java.time.OffsetDateTime;
import java.util.List;

import au.com.digitalpurpose.dctest.server.request.meatfreeday.CreateMeatFreeDayRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.digitalpurpose.dctest.server.exception.NoSuchObjectException;
import au.com.digitalpurpose.dctest.server.model.MeatFreeDay;
import au.com.digitalpurpose.dctest.server.model.UserAccount;
import au.com.digitalpurpose.dctest.server.model.UserDevice;
import au.com.digitalpurpose.dctest.server.repository.MeatFreeDayRepository;
import au.com.digitalpurpose.dctest.server.repository.UserDeviceRepository;
import au.com.digitalpurpose.dctest.server.security.Caller;

@Service
public class MeatFreeDayService {
    private static final Logger log = LoggerFactory.getLogger(MeatFreeDayService.class);

    private final MeatFreeDayRepository meatFreeDayRepository;
    private final UserDeviceRepository userDeviceRepository;

    public MeatFreeDayService(MeatFreeDayRepository meatFreeDayRepository,
                              UserDeviceRepository userDeviceRepository) {
        this.meatFreeDayRepository = meatFreeDayRepository;
        this.userDeviceRepository = userDeviceRepository;
    }

    public List<MeatFreeDay> getMfdsForUser(String userIdentifier,
                                            UserAccount caller) {

        // TODO: security checks so only admins or users themselves can query the users data (requires logins for users)

        return meatFreeDayRepository.findForUser(userIdentifier);
    }

    @Transactional(rollbackFor = Exception.class)
    public MeatFreeDay createMeatFreeDay(String userIdentifier,
                                         CreateMeatFreeDayRequest request,
                                         UserAccount caller) throws NoSuchObjectException {

        // TODO: security checks so only admins or users themselves can add mfds for a user (requires logins for users)

        UserDevice userDevice = userDeviceRepository.findByIdentifier(userIdentifier).orElseThrow(() -> new NoSuchObjectException(UserDevice.class, userIdentifier));

        // TODO: checks:
        // - only one MFD per calendar date and user

        OffsetDateTime date = request.getDate() != null ? request.getDate() : OffsetDateTime.now();
        MeatFreeDay mfd = new MeatFreeDay(date, userDevice);
        mfd = meatFreeDayRepository.save(mfd);
        return mfd;
    }

    public void deleteMeatFreeDay(Long mfdId,
                                  UserAccount caller) {

        // TODO: security checks so only admins or users themselves can delete mfds for a user (requires logins for users)

        meatFreeDayRepository.deleteById(mfdId);
    }

}
