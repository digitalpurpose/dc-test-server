package au.com.digitalpurpose.dctest.server.request.user;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UpdatePasswordRequest extends ChangePasswordRequest {

    @NotNull
    private Long id;

    @NotBlank(message = "Current password field is required")
    private String oldPassword;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

}
